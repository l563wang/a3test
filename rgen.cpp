#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[]){
int N = 25;
int s_k = 10; //The number of streets is a random integer in [2,s_k]
int n_k = 5; //The number of line-segments in each street is a random integer in [1,k]
int l_k = 5; //wait a random number w seconds where w is in[5,l_k] before generating the next (random) input
int c_k = 20; //generate (x,y) coordinates such that every x and y value is in range [-k,k]
int i = 1;
int k;
while (argc > 1){
char line = **(argv+i);
char command = *(*(argv+i)+1);
k = atoi(argv[i+1]);
switch(command){
case 's':
s_k = k;
break;
case 'n':
n_k = k;
break;
case 'l':
l_k = k;
break;
case 'c':
c_k = k;
break;
}
argc = argc - 2;
i = i + 2;
}

int s_r = arc4random_uniform(s_k-1)+2;
int n_r = arc4random_uniform(n_k)+1;
int w = arc4random_uniform(l_k-4)+5;
int x = arc4random_uniform(2*c_k+1)-c_k;
int y = arc4random_uniform(2*c_k+1)-c_k;

cout << s_r << n_r << w << x << y <<endl;

return 0;
}